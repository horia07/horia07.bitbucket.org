portfolio, programmer, developer, computer science
-------------------------------
Languages:		Java, Python, C#, C/C++, PHP, SQL, Javascript, CoffeeScript, jQuery

Methodologies:		OOP, Design Patterns, UML, Scrum, Extreme Programming

Operating systems:	Linux, Windows, Android

Databases:		MySQL, PostgreSQL, SQLite, Oracle, SQL Server, Access, Excel

Tools:			Apache, Nginx, Tomcat | Git, SVN, Mercurial
